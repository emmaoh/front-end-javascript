const triviaToHtml = (category, question, answer) => {
    return `
    <h3>${category}</h3
    <details>
        <summary>${question}</summary>
        ${answer}
    <details>
    `;
}

const div = document.querySelector('#trivia')

const url = 'https://jservice.xyz/api/random-clue?valid=true';
const response = await fetch(url);
console.log({response})
if(response.ok){
    const data = await response.json()
    console.log({data})

    // const question = data.question;
    // const answer = data.answer;

    const {question, answer} = data;
    const category = data.category.title;

    const trivia = triviaToHtml(category, question, answer);
    div.innerHTML = trivia;
}else{
    div.innerHTML = 'Something went wrong, please come back later'
}

